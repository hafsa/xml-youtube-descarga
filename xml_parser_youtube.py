#!/usr/bin/python3

#
# Simple XML parser for JokesXML
# Jesus M. Gonzalez-Barahona
# jgb @ gsyc.es
# SARO and SAT subjects (Universidad Rey Juan Carlos)
# 2009-2020
#
# Just prints the jokes in a JokesXML file
import urllib
from urllib import request
from xml.sax.handler import ContentHandler #SAX: NO ALMACENA
from xml.sax import make_parser
import sys
import string


videos_yt = ""
class CounterHandler(ContentHandler):

    def __init__(self):
        self.inContent = False
        self.inVideo = False
        self.theContent = ""
        self.title = ""
        self.link = ""

    def startElement(self, name, attrs):
        if name == 'entry':
            self.inContent = True
        elif self.inContent:
            if name == 'title':
                self.inVideo = True
            elif name == 'link':
                self.link = attrs.get('href')

    def endElement(self, name):
        global videos_yt
        if name == 'entry':
            self.inContent = False
            videos_yt = videos_yt + "<li><a href='" + self.link + "'>" + self.title + "</a></li>\n"

        elif self.inContent:
            if name == 'title':
                self.title = self.theContent
                self.theContent = ""
                self.inContent = False


    def characters(self, chars):
        if self.inVideo:
            self.theContent = self.theContent + chars

YoutubeParser = make_parser() #PARSER TE VA LEYENDO LINEA A LINEA E IDENTIFICANDO QUE HAY (SINTÁCTICA)
YoutubeHandler = CounterHandler() #EL HANDLER SABE DE SEMANTICA FORMADO POR 4 METODOS EN ESTE CASO
YoutubeParser.setContentHandler(YoutubeHandler)

# --- Main program
if __name__ == "__main__":
    PAGE = """
        <!DOCTYPE html>
        <html lang="en">
          <body>
            <h1>Channel contents:</h1>
            <ul>
        {videos_yt}
            </ul>
          </body>
        </html>
        """

    if len(sys.argv) < 2:
        print("Usage: python xml-parser-youtube.py <identificador>")
        print()
        print(" <document>: file name of the document to parse")
        sys.exit(1)

    url = "https://www.youtube.com/feeds/videos.xml?channel_id=" + sys.argv[1]
    youtubexml = urllib.request.urlopen(url)
    YoutubeParser.parse(youtubexml)
    page = PAGE.format(videos_yt=videos_yt)
    print(page)

    # Ready, set, go!
    """xmlFile = open(sys.argv[1], "r")
    YoutubeHandler.parse(xmlFile)"""

    print("Parse complete")